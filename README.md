# README #

This repository contains ebooks and papers I find interesting, divided by category. All the material included here is freely available on Google Scholar or on the author's website.